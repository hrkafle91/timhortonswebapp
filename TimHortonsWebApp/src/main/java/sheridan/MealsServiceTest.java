package sheridan;

import static org.junit.Assert.*;

import sheridan.MealType;
import sheridan.MealsService;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testDrinksRegular() throws Exception{
        List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
        
		assertTrue("Invalid value for mealType", (!types.isEmpty()));
	}
	
	@Test
	public void testDrinksException() throws Exception{
		MealType type = null;
        List<String> types = MealsService.getAvailableMealTypes(type);
        
		assertFalse("Invalid value for mealType", (types.get(0) != "No Brand Available"));
	}
	
	@Test
	public void testDrinksBoundaryIn() throws Exception{
        List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
        
		assertTrue("Invalid value for mealType", (types.size() > 3));
	}
	
	@Test
	public void testDrinksBoundaryOut() throws Exception{
		MealType type = null;
        List<String> types = MealsService.getAvailableMealTypes(type);
        
		assertFalse("Invalid value for mealType", (types.size() != 1));
	}
}
